class Calculadora {
    float n1;
    float n2;

    float suma () { return n1 + n2;}
    float resta () { return n1 - n2;}
    float multiplicacion () { return n1 * n2;}
    float division () { return n2 != 0 ? n1 / n2 : 0;}
}

System.in.withReader {
    Calculadora calculadora = new Calculadora();
    print 'Introduzca operador: ';
    char operacion = it.readLine() as char;
    print 'Introduzca primer numero: ';
    float num1 = it.readLine() as float;
    print 'Introduzca segundo numero: ';
    float num2 = it.readLine() as float;
    float resultado = 0;
    calculadora.n1 = num1;
    calculadora.n2 = num2;
    println "Realizando la operacion ${num1} ${operacion} ${num2}"
    switch(operacion) {
      case '+' : resultado = calculadora.suma(); break;
      case '-' : resultado = calculadora.resta(); break;
      case '*' : resultado = calculadora.multiplicacion(); break;
      case '/' : resultado = calculadora.division(); break;
    }
    println "Resultado: ${resultado}";
}

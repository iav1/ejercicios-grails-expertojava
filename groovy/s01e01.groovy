class Todo {
    String titulo = '';
    String descripcion = '';
}

ArrayList todos = new ArrayList();
todos.add(new Todo(titulo : 'Lavadora', descripcion : 'Poner lavadora'));
todos.add(new Todo(titulo : 'Impresora', descripcion : 'Comprar cartuchos impresora'));
todos.add(new Todo(titulo : 'Peliculas', descripcion : 'Devolver películas videoclub'));

todos.each{uno -> println "${uno.getTitulo()} ${uno.getDescripcion()}"};

class Libro {
    /* Añade aquí la definición de la clase */
    String nombre;
    Integer anyo;
    String autor;
    String editorial;

    String getAutor() {
        def partes = autor.split(',');
        String correcto = "${partes[1].trim()} ${partes[0].trim()}";
        return correcto;
    }
}

/* Crea aquí las tres instancias de libro l1, l2 y l3 */
Libro l1 = new Libro(nombre: 'La colmena', anyo: 1951, autor: 'Cela Trulock, Camilo José');
Libro l2 = new Libro(nombre: 'La galatea', anyo: 1585, autor: 'de Cervantes Saavedra, Miguel');
Libro l3 = new Libro(nombre: 'La dorotea', anyo: 1632, autor: 'Lope de Vega y Carpio, Félix Arturo');

assert l1.getNombre() == 'La colmena';
assert l2.getNombre() == 'La galatea';
assert l3.getNombre() == 'La dorotea';

assert l1.getAnyo() == 1951;
assert l2.getAnyo() == 1585;
assert l3.getAnyo() == 1632;

assert l1.getAutor() == 'Camilo José Cela Trulock';
assert l2.getAutor() == 'Miguel de Cervantes Saavedra';
assert l3.getAutor() == 'Félix Arturo Lope de Vega y Carpio';

/* Añade aquí la asignación de la editorial a todos los libros */

l1.setEditorial('Anaya');
l2.setEditorial('Planeta');
l3.setEditorial('Santillana');

assert l1.getEditorial() == 'Anaya'
assert l2.getEditorial() == 'Planeta'
assert l3.getEditorial() == 'Santillana'

/* Añade aquí la implementación del factorial en un closure */

/* long n-> return n>1 ? n * facRec(n - 1) : 1 */
def factorial = {
    long numero ->
    { -> numero > 1 ? numero * owner.call( numero - 1 ) : 1 }()
};

assert factorial(1)==1
assert factorial(2)==1*2
assert factorial(3)==1*2*3
assert factorial(4)==1*2*3*4
assert factorial(5)==1*2*3*4*5
assert factorial(6)==1*2*3*4*5*6
assert factorial(7)==1*2*3*4*5*6*7
assert factorial(8)==1*2*3*4*5*6*7*8
assert factorial(9)==1*2*3*4*5*6*7*8*9
assert factorial(10)==1*2*3*4*5*6*7*8*9*10

/* Añade a partir de aquí el resto de tareas solicitadas en el ejercicios */

/* Listado de factoriales de números*/

[3,5,6,8,12, 16].each{ numero -> println "El factorial de ${numero} es ${factorial(numero)}" };

/* Metodos Ayer y Mañana*/

def yesterday = { Date fecha -> fecha.previous();};

def tomorrow = { Date fecha -> fecha.next();};

Date hoy = new Date();
println "Hoy es ${hoy.getDateString()}, ayer fue ${yesterday(hoy).getDateString()} mañana es ${tomorrow(hoy).getDateString()}";

[
    new Date().parse("d/M/yyyy","28/2/2016"),
    new Date().parse("d/M/yyyy","31/12/2014"),
    new Date().parse("d/M/yyyy","1/1/2015"),
    new Date().parse("d/M/yyyy","30/8/2015"),
    new Date().parse("d/M/yyyy","30/4/2012"),
    new Date().parse("d/M/yyyy","28/2/2015"),
    new Date().parse("d/M/yyyy","1/4/2008")
].each{ fecha -> println "Hoy es ${fecha.getDateString()}, ayer fue ${yesterday(fecha).getDateString()} mañana es ${tomorrow(fecha).getDateString()}" };

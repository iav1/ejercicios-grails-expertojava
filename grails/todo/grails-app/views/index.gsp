<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<r:require modules="font-awesome"/>
		<title>Welcome to Grails</title>
		<style type="text/css" media="screen">
			.danger_msg {
				color: red;
			}

			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}
		</style>
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="status" role="complementary">
			<asset:image src="avatar.jpg" alt="Grails"/>
		</div>
		<div id="page-body" role="main">
			<h1>Bienvenido a la aplicacion TODO</h1>
			<p>Gracias a esta aplicac&oacute;n podr&aacute;s gestionar todas las tareas y organizar por listas- Estas son algunas de las acciones que puedes realizar por el momento:</p>

			<div id="controller-list" role="navigation">
				<ul>
					<sec:access expression="hasAnyRole('ROLE_BASIC','ROLE_ADMIN')">

						<sec:ifAnyGranted roles="ROLE_ADMIN">
							<li class="controller"><g:link controller="category">Gesti&oacute;n de categor&iacute;as</g:link></li>
							<li class="controller"><g:link controller="tag">Gesti&oacute;n de etiquetas</g:link></li>
							<li class="controller"><g:link controller="User">Gestionar Usuarios</g:link></li>
						</sec:ifAnyGranted>

						<sec:ifAnyGranted roles="ROLE_BASIC">
							<li class="controller"><g:link controller="todo">Gesti&oacute;n de tareas</g:link></li>
						</sec:ifAnyGranted>
					</sec:access>
					<sec:noAccess expression="hasAnyRole('ROLE_BASIC','ROLE_ADMIN')">
						<li><i class="fa fa-exclamation-triangle danger_msg"></i> <strong>Debe <g:link controller='login' action='auth'>loguearse</g:link> para poder realizar alguna acci&oacute;n.</strong> <i class="fa fa-exclamation-triangle danger_msg"></i></li>
					</sec:noAccess>
				</ul>
			</div>
		</div>
	</body>
</html>

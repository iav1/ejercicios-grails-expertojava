<div id="menu">
    <nobr>
        <sec:ifNotLoggedIn>
            <g:link controller='login' action='auth'>
                Login
            </g:link>
        </sec:ifNotLoggedIn>
        <sec:ifLoggedIn>
            <b>Bienvenido <sec:loggedInUserInfo field="username"/> </b>
            <g:form controller="logout" action="index" method="POST" >
                <g:actionSubmit class="button" action="index" value="Logout"/>
            </g:form>
        </sec:ifLoggedIn>
    </nobr>
</div>

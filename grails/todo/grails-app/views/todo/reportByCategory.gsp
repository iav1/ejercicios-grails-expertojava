
<%@ page import="es.ua.expertojava.todo.Category" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="list-category" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="name" title="${message(code: 'category.name.label', default: 'Name')}" />
					</tr>
				</thead>
				<tbody>
					<g:each in="${todoInstanceList}" status="i" var="todoInstance">
						<tr class="${(i % 2) == 0 ? 'even' : 'odd'}" >
							<td style="background-color:${todoInstance.color};max-width:40px;"></td>

							<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>

							<td>${fieldValue(bean: todoInstance, field: "description")}</td>

							<td><g:formatDate date="${todoInstance.date}" /></td>

							<td><g:formatDate date="${todoInstance.reminderDate}" /></td>

							<td>${fieldValue(bean: todoInstance, field: "url")}</td>

							<td><g:formatBoolean boolean="${todoInstance.done}" /></td>

						</tr>
					</g:each>
				</tbody>
			</table>
		</div>
	</body>
</html>

<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="report" action="report"><g:message code="default.report.label" args="[entityName]" /></g:link></li>
				<!-- <li><g:link class="create" controller="searchableMod" action="index"><g:message code="default.buscador.label" default="Buscador"/></g:link></li> -->
				<li>
					<g:form url="[resource:todoInstance, action:'searchTodos']" >
						<fieldset class="form">
							<g:textField name="query" value="${params.query}"/>
							<g:submitButton name="submit" value="Search" />
						</fieldset>
					</g:form>
				</li>
			</ul>
		</div>
		<div id="list-todo" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
						<g:sortableColumn property="edit" title="Edit" style="text-align:center"/>

						<g:sortableColumn property="delete" title="Delete" style="text-align:center"/>

						<g:sortableColumn property="title" title="${message(code: 'todo.title.label', default: 'Title')}" />


						<g:sortableColumn property="date" title="${message(code: 'todo.date.label', default: 'Date')}"style="text-align:center" />


						<g:sortableColumn property="url" title="${message(code: 'todo.url.label', default: 'Url')}" />

						<g:sortableColumn property="done" title="${message(code: 'todo.done.label', default: 'Done')}" style="text-align:center"/>

					</tr>
				</thead>
				<tbody>
				<g:each in="${todoInstanceList}" status="i" var="todoInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

						<td style="text-align:center">
							<g:link class="edit" action="edit" resource="${todoInstance}"><button><i class="fa fa-pencil"></i></button></g:link>
						</td>
						<td style="text-align:center">
							<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
							<!--
								<g:link class="delete" action="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-times"></i></g:link>
							-->
								<button type="submit" class="delete" value="Save" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"><i class="fa fa-times"></i></button>
							</g:form>
						</td>

						<td><g:link action="show" id="${todoInstance.id}">${fieldValue(bean: todoInstance, field: "title")}</g:link></td>


						<td style="text-align:center"><g:formatDate format="dd-MM-yyyy" date="${todoInstance.date}"/></td>


						<td>${fieldValue(bean: todoInstance, field: "url")}</td>

						<td style="text-align:center"><todo:printIconFromBoolean value="${todoInstance.done}" /></td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${todoInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>


<%@ page import="es.ua.expertojava.todo.Todo" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'todo.label', default: 'Todo')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-todo" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
				<li><g:link class="report" action="report"><g:message code="default.report.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-todo" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list todo">
				<g:if test="${todoInstance?.title}">
				<li class="fieldcontain">
					<span id="title-label" class="property-label"><g:message code="todo.title.label" default="Title" /></span>
					<span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${todoInstance}" field="title"/></span>
				</li>
				</g:if>

				<g:if test="${todoInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="todo.description.label" default="Description" /></span>

						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${todoInstance}" field="description"/></span>

				</li>
				</g:if>

				<g:if test="${todoInstance?.date}">
				<li class="fieldcontain">
					<span id="date-label" class="property-label"><g:message code="todo.date.label" default="Date" /></span>

						<span class="property-value" aria-labelledby="date-label"><g:formatDate format="dd-MM-yyyy" date="${todoInstance?.date}" /></span>

				</li>
				</g:if>

				<g:if test="${todoInstance?.reminderDate}">
				<li class="fieldcontain">
					<span id="reminderDate-label" class="property-label"><g:message code="todo.reminderDate.label" default="Reminder Date" /></span>

						<span class="property-value" aria-labelledby="reminderDate-label"><g:formatDate format="dd-MM-yyyy" date="${todoInstance?.reminderDate}" /></span>

				</li>
				</g:if>

				<g:if test="${todoInstance?.url}">
				<li class="fieldcontain">
					<span id="url-label" class="property-label"><g:message code="todo.url.label" default="Url" /></span>

						<span class="property-value" aria-labelledby="url-label"><g:fieldValue bean="${todoInstance}" field="url"/></span>

				</li>
				</g:if>

				<g:if test="${todoInstance?.done}">
				<li class="fieldcontain">
					<span id="done-label" class="property-label"><g:message code="todo.done.label" default="Done" /></span>
					<span class="property-value" aria-labelledby="done-label"><g:formatBoolean boolean="${todoInstance?.done}" /></span>
				</li>
				</g:if>

				<g:if test="${todoInstance?.category}">
				<li class="fieldcontain">
					<span id="category-label" class="property-label"><g:message code="todo.category.label" default="Category" /></span>
					<span class="property-value" aria-labelledby="category-label">
						${todoInstance?.category?.encodeAsHTML()}
						<!-- <g:link controller="category" action="show" id="${todoInstance?.category?.id}">${todoInstance?.category?.encodeAsHTML()}</g:link> -->
					</span>
				</li>
				</g:if>

				<g:if test="${todoInstance?.tags.size() > 0}">
				<li class="fieldcontain">
					<span id="tags-label" class="property-label"><g:message code="todo.tags.label" default="Tags" /></span>
					<g:each in="${todoInstance?.tags}" status="i" var="tag">
						<span class="property-value" aria-labelledby="tags-label" style="background-color:${tag?.color};padding: 10px 0px 10px 0px">${tag}</span>
					</g:each>
				</li>
				</g:if>

			</ol>
			<g:form url="[resource:todoInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${todoInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>

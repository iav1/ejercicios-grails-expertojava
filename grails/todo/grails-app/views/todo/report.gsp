
<%@ page import="es.ua.expertojava.todo.Category" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-category" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div id="list-category" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
						<g:sortableColumn property="name" title="${message(code: 'category.name.label', default: 'Name')}" />
					</tr>
				</thead>
				<tbody>
					<g:form url="[resource:todoInstance, action:'reportByCategory']" >
						<fieldset class="form">
							<g:each in="${categoryInstanceList}" status="i" var="categoryInstance">
								<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
									<td><g:checkBox name="selectedCategories" value="${fieldValue(bean: categoryInstance, field: "id")}" checked="true"/><label>${fieldValue(bean: categoryInstance, field: "name")}</label></td>
								</tr>
							</g:each>
						</fieldset>
						<fieldset class="buttons">
							<g:submitButton name="submit" value="Filtrar" />
						</fieldset>
					</g:form>
				</tbody>
			</table>
		</div>
	</body>
</html>

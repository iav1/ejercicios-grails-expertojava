<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
	</head>
	<body>
		<div id="delete-tag" class="content" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:form url="[resource:tagInstance, action:'delete']" method="DELETE">
				<fieldset class="form">
					<asset:image src="confirm.jpg" alt="Confirm deletion logo" style="width:100px"/>
					<h3>Are you sure?</h3>
				</fieldset>
				<fieldset class="buttons">
					<g:link class="success" action="delete" resource="${tagInstance}"><g:message code="default.button.confirm.label" default="YES" /></g:link>
					<g:link class="error" action="show" resource="${tagInstance}"><g:message code="default.button.cancel.label" default="NO" /></g:link>
				</fieldset>
			</g:form>
		</div>
	</body>
</html>

package es.ua.expertojava.todo

class PrintIconFromBooleanTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = 'todo';

    def printIconFromBoolean = { attrs ->
        boolean done = attrs['value'];
        
        out << "<i class='fa fa-${ done ? "check" : "close"}' style='color:${ done ? "green" : "red" }'></i>";
    }
}

package es.ua.expertojava.todo

import grails.plugin.springsecurity.annotation.Secured

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Secured(['ROLE_BASIC'])
@Transactional(readOnly = true)
class TodoController {

    def searchableService

    def TodoService todoService = new TodoService();

    def TagService tagService = new TagService();

    def springSecurityService;

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def user = springSecurityService.currentUser;
        respond Todo.findAllByUser(user, params), model:[todoInstanceCount: Todo.count()]
    }

    def show(Todo todoInstance) {
        if(todoInstance != null) todoInstance.tags.each { println it;}
        respond todoInstance;
    }

    def create() {
        params.user = springSecurityService.currentUser;
        respond new Todo(params)
    }

    def report() {
        respond Category.list()
    }

    def reportByCategory(params) {
        def selectedCategories = params.selectedCategories.collect{it.toLong()};
        respond Todo.findAllByCategoryInList(Category.getAll(selectedCategories), ["sort":"date", "order":"asc"]);
    }

    @Transactional
    def save(Todo todoInstance) {
        todoInstance.user = springSecurityService.currentUser;
        todoInstance.validate();

        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'create'
            return
        }

        todoService.saveTodo(todoInstance);

        todoInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*' { respond todoInstance, [status: CREATED] }
        }
    }

    def edit(Todo todoInstance) {
        respond todoInstance
    }

    @Transactional
    def update(Todo todoInstance) {
        if (todoInstance == null) {
            notFound()
            return
        }

        if (todoInstance.hasErrors()) {
            respond todoInstance.errors, view:'edit'
            return
        }

        todoService.saveTodo(todoInstance);

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect todoInstance
            }
            '*'{ respond todoInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Todo todoInstance) {

        if (todoInstance == null) {
            notFound()
            return
        }

        todoService.removeTodo(todoInstance)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Todo.label', default: 'Todo'), todoInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    def listNextTodos(Integer days) {
        respond todoService.getNextTodos(days, params),
            model:[todoInstanceCount: todoService.countNextTodos(days)],
            view:"index"
    }

    @Secured(['ROLE_ADMIN'])
    def showTodosByUser(){
        def user = User.findByUsername(params.username)
        respond Todo.findAllByUser(user);
    }

    def searchTodos = {
        def query = params.query
        if(query){
            def user = springSecurityService.currentUser;
            def userTodos = Todo.findAllByUser(user)
            def todos = searchableService.search('*' + query + '*')
            render(view: "index", model: [todoInstanceList: Todo.findAllByTitleInList(todos.results).intersect(userTodos)])
        }else{
            redirect(action: "index")
        }
    }

}

package es.ua.expertojava.todo

class Tag {

    String name
    String color

    static hasMany = [todos:Todo]

    static constraints = {
        name(blank:false, nullable:false, unique:true)
        color(nullable:false, shared:'RGBcolor', unique:true)
    }

    String toString(){
    	name
    }
}

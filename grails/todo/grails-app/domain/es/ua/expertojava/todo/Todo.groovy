package es.ua.expertojava.todo

class Todo {
    User user
    String title
    String description
    Date date
    Date reminderDate
    String url
    boolean done
    Category category
    Date dateCreated
    Date lastUpdated
    Date dateDone
    ArrayList<Tag> tags = new ArrayList<Tag>();
    static searchable = [only: 'title']

    static constraints = {
        user(nullable:false)
        title(blank:false)
        description(blank:true, nullable:true, maxSize:1000)
        date(nullable:false)
        reminderDate(nullable:true,
        validator: { val, obj ->
            Date now = new Date();
            if (val) {
                 if(obj?.date) {
                     return val.before(obj.date) && val.after(now);
                 } else {
                     return val.after(now);
                 }
            }
            return true;
        })
        url(nullable:true, url:true)
        done(nullable:false, done:false)
        category(nullable:true)
        dateDone(nullable:true)
    }

    String toString(){
        title
    }

    void addToTags(Tag tag) {
        tags.add(tag);
    }
}

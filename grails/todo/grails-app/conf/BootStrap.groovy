import es.ua.expertojava.todo.*

class BootStrap {

    def init = { servletContext ->

        def role_basic = new Role(authority: "ROLE_BASIC").save();
        def role_admin = new Role(authority: "ROLE_ADMIN").save();

        def administrador = new User(username: "admin", password: "admin", name: "Administrador", surnames: "Del mundo mundial", email: "administrador@todo.expertojava.ua.es", confirmPassword: false).save();
        def usuario1 = new User(username: "usuario1", password: "usuario1", name: "Usuario", surnames: "Uno", email: "usuarioUno@todo.expertojava.ua.es", confirmPassword: false).save();
        def usuario2 = new User(username: "usuario2", password: "usuario2", name: "Usuario", surnames: "Dos", email: "usuarioDos@todo.expertojava.ua.es", confirmPassword: false).save();

        PersonRole.create administrador, role_admin, true
        PersonRole.create usuario1, role_basic, true
        PersonRole.create usuario2, role_basic, true

        def categoryHome = new Category(name:"Hogar").save()
        def categoryJob = new Category(name:"Trabajo").save()

        def tagEasy = new Tag(name:"Fácil", color:"#00FF00").save()
        def tagDifficult = new Tag(name:"Difícil", color:"#FF0000").save()
        def tagArt = new Tag(name:"Arte", color:"#FFAAAA").save()
        def tagRoutine = new Tag(name:"Rutina", color:"#0000FF").save()
        def tagKitchen = new Tag(name:"Cocina", color:"#FFCC00").save()

        def todoPaintKitchen = new Todo(user: usuario1, title:"Pintar cocina", date:new Date()+1)
        def todoCollectPost = new Todo(user: usuario1, title:"Recoger correo postal", date:new Date()+2)
        def todoBakeCake = new Todo(user: administrador, title:"Cocinar pastel", date:new Date()+4)
        def todoWriteUnitTests = new Todo(user: usuario2, title:"Escribir tests unitarios", date:new Date())

        todoPaintKitchen.addToTags(tagDifficult)
        todoPaintKitchen.addToTags(tagArt)
        todoPaintKitchen.addToTags(tagKitchen)
        todoPaintKitchen.category = categoryHome
        todoPaintKitchen.save()

        todoCollectPost.addToTags(tagRoutine)
        todoCollectPost.category = categoryJob
        todoCollectPost.save()

        todoBakeCake.addToTags(tagEasy)
        todoBakeCake.addToTags(tagKitchen)
        todoBakeCake.category = categoryHome
        todoBakeCake.save()

        todoWriteUnitTests.addToTags(tagEasy)
        todoWriteUnitTests.category = categoryJob
        todoWriteUnitTests.save()
    }
    def destroy = {
    }
}

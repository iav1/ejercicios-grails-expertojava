package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class TagService {

    def serviceMethod() {

    }

    @Transactional
    def delete(Tag tagInstance) {

        if (tagInstance == null) {
            notFound()
            return
        }

        // 'Borrado' de tareas asociadas (las pone a null)
        tagInstance.hasMany.each { it = null;}

        tagInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Tag.label', default: 'Tag'), tagInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}

package es.ua.expertojava.todo

import grails.transaction.Transactional

@Transactional
class CategoryService {

    def serviceMethod() {

    }

    @Transactional
    def delete(Category categoryInstance) {

        if (categoryInstance == null) {
            notFound()
            return
        }

        // 'Borrado' de tareas asociadas (las pone a null)
        categoryInstance.hasMany.each { it = null;}

        categoryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Category.label', default: 'Category'), categoryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }
}

package es.ua.expertojava.todo

class TodoService {

    def serviceMethod() {

    }

    def saveTodo(Todo todoInstance) {
        if(todoInstance?.done) {
            todoInstance.dateDone = new Date();
        }
        todoInstance.save flush:true
        return todoInstance;
    }

    def removeTodo(Todo todoInstance) {
        def tags = todoInstance.tags
        tags.each {tag -> tag.todos.remove(todoInstance)}
        todoInstance.delete()
    }

    def lastTodosDone(Integer hours) {
        Date since = new Date(new Date() - TimeUnit.HOURS.toMillis(hours));
        return Todo.findAllByDateDoneThan(since);
    }

    def countNextTodos(Integer days) {
        Date begin = new Date(System.currentTimeMillis())
        Date end = begin + days
        Todo.countByDateBetween(begin, end)
    }

    def getNextTodos(Integer days, params) {
        Date begin = new Date(System.currentTimeMillis())
        Date end = begin + days
        Todo.findAllByDateBetween(begin, end, params)
    }
}

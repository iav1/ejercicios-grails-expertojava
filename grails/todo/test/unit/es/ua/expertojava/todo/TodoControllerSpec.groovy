package es.ua.expertojava.todo



import grails.test.mixin.*
import spock.lang.*

@TestFor(TodoController)
@Mock(Todo)
class TodoControllerSpec extends Specification {

    def todoService = new TodoService();

    def setup () {
        def usuario = new User(username: "usuarioTest", password: "usuarioTest", name: "Usuario", surnames: "Test", email: "usuarioTest@todo.expertojava.ua.es", confirmPassword: false);
        controller.springSecurityService = [currentUser : usuario];
        controller.todoService = todoService;
    }

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
        params["user"] = controller.springSecurityService.currentUser;
        params["title"] = 'tarea01';
        params["description"] = "Tarea de prueba"
        params["date"] = new Date() + 10;
        params["reminderDate"] = new Date() + 5;
        params["description"] = 'Description name'
        params["dateDone"] = new Date() + 8;
        params["done"] = true;
    }

    void "Test the index action returns the correct model"() {

        when:"The index action is executed"
            controller.index()

        then:"The model is correct"
            !model.todoInstanceList
            model.todoInstanceCount == 0
    }

    void "Test the create action returns the correct model"() {
        when:"The create action is executed"
            controller.create()

        then:"The model is correctly created"
            model.todoInstance!= null
    }

    void "Test the save action correctly persists an instance"() {

        when:"The save action is executed with an invalid instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'POST'
            def todo = new Todo()
            todo.validate()
            controller.save(todo)

        then:"The create view is rendered again with the correct model"
            model.todoInstance!= null
            view == 'create'

        when:"The save action is executed with a valid instance"
            response.reset()
            populateValidParams(params)
            populateValidParams(params)
            todo = new Todo(params)
            controller.save(todo)

        then:"A redirect is issued to the show action"
            response.redirectedUrl == '/todo/show/1'
            controller.flash.message != null
            Todo.count() == 1
    }

    void "Test that the show action returns the correct model"() {
        when:"The show action is executed with a null domain"
            controller.show(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the show action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.show(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test that the edit action returns the correct model"() {
        when:"The edit action is executed with a null domain"
            controller.edit(null)

        then:"A 404 error is returned"
            response.status == 404

        when:"A domain instance is passed to the edit action"
            populateValidParams(params)
            def todo = new Todo(params)
            controller.edit(todo)

        then:"A model is populated containing the domain instance"
            model.todoInstance == todo
    }

    void "Test the update action performs an update on a valid domain instance"() {
        when:"Update is called for a domain instance that doesn't exist"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'PUT'
            controller.update(null)

        then:"A 404 error is returned"
            response.redirectedUrl == '/todo/index'
            flash.message != null


        when:"An invalid domain instance is passed to the update action"
            response.reset()
            def todo = new Todo()
            todo.validate()
            controller.update(todo)

        then:"The edit view is rendered again with the invalid instance"
            view == 'edit'
            model.todoInstance == todo

        when:"A valid domain instance is passed to the update action"
            response.reset()
            populateValidParams(params)
            todo = new Todo(params).save(flush: true)
            controller.update(todo)

        then:"A redirect is issues to the show action"
            response.redirectedUrl == "/todo/show/$todo.id"
            flash.message != null
    }

    void "Test that the delete action deletes an instance if it exists"() {
        when:"The delete action is called for a null instance"
            request.contentType = FORM_CONTENT_TYPE
            request.method = 'DELETE'
            controller.delete(null)

        then:"A 404 is returned"
            response.redirectedUrl == '/todo/index'
            flash.message != null

        when:"A domain instance is created"
            response.reset()
            populateValidParams(params)
            def todo = new Todo(params).save(flush: true)

        then:"It exists"
            Todo.count() == 1

        when:"The domain instance is passed to the delete action"
            controller.delete(todo)

        then:"The instance is deleted"
            Todo.count() == 0
            response.redirectedUrl == '/todo/index'
            flash.message != null
    }

    void "Test de listNextTodos" () {
        given: "Creamos las tareas necesarias para la prueba"
            def todo1 = new Todo(user: controller.springSecurityService.currentUser, title:"Tarea 1", description:"Description de tarea 1", date: new Date() + 1, done: false)
            def todo2 = new Todo(user: controller.springSecurityService.currentUser, title:"Tarea 2", description:"Description de tarea 2", date: new Date() + 2, done: false)
            def todo3 = new Todo(user: controller.springSecurityService.currentUser, title:"Tarea 3", description:"Description de tarea 3", date: new Date() + 3, done: false)
            def todo4 = new Todo(user: controller.springSecurityService.currentUser, title:"Tarea 4", description:"Description de tarea 4", date: new Date() + 4, done: false)
            def todo5 = new Todo(user: controller.springSecurityService.currentUser, title:"Tarea 5", description:"Description de tarea 5", date: new Date() + 5, done: false)

           when: "Persistimos las tareas"
           todo1.save(flush: true)
           todo2.save(flush: true)
           todo3.save(flush: true)
           todo4.save(flush: true)
           todo5.save(flush: true)

           controller.listNextTodos(dias)

           then: "Realizamos las pruebas"
           view == 'index'
           model.todoInstanceCount == resultado

           where: "Relacionamos los datos esperados a las peticiones hechas"
           dias    |   resultado
           1       |   1
           2       |   2
           3       |   3
           4       |   4
           5       |   5

       }

       def "Listados Por categorias"() {

           given: "Genera las categorias para la prueba"
           def c1 = new Category(name:"Categoria1")
           def c2 = new Category(name:"Categoria2")
           def c3 = new Category(name:"Categoria3")

           when: "Persiste los ejemplos y obtengo el listado"
           c1.save(flush: true)
           c2.save(flush: true)
           c3.save(flush: true)

           controller.report()

           then:"Comprueba si se devuelven las categorias creadas"

           model.categoryInstanceList.size() == 3

       }
}

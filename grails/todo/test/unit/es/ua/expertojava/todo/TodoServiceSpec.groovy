package es.ua.expertojava.todo

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification


@TestFor(TodoService)
class TodoServiceSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "El método getNextTodos devuelve los siguientes todos de los días pasado por parámetro"() {
        given:
            def usuario = new User(username: "usuarioTest", password: "usuarioTest", name: "Usuario", surnames: "Test", email: "usuarioTest@todo.expertojava.ua.es", confirmPassword: false)
            def todoDayBeforeYesterday = new Todo(user: usuario, title:"Todo day before yesterday", date: new Date() - 2, done : true)
            def todoYesterday = new Todo(user: usuario, title:"Todo yesterday", date: new Date() - 1 , done : true)
            def todoToday = new Todo(user: usuario, title:"Todo today", date: new Date(), done : true)
            def todoTomorrow = new Todo(user: usuario, title:"Todo tomorrow", date: new Date() + 1 , done : false)
            def todoDayAfterTomorrow = new Todo(user: usuario, title:"Todo day after tomorrow", date: new Date() + 2 , done : false)
            def todoDayAfterDayAfterTomorrow = new Todo(user: usuario, title:"Todo day after tomorrow", date: new Date() + 3 , done : false)
        and:
            mockDomain(Todo,[todoDayBeforeYesterday, todoYesterday, todoToday, todoTomorrow, todoDayAfterTomorrow, todoDayAfterDayAfterTomorrow])
        and:
            def nextTodos = service.getNextTodos(2,[:])
        expect:
            Todo.count() == 6
        and:
            nextTodos.containsAll([todoTomorrow, todoDayAfterTomorrow])
            nextTodos.size() == 2
        and:
            !nextTodos.contains(todoDayBeforeYesterday)
            !nextTodos.contains(todoToday)
            !nextTodos.contains(todoYesterday)
            !nextTodos.contains(todoDayAfterDayAfterTomorrow)
    }

    void "Test Metodo SaveTodo que compruebe el almacenamiento de campos correctamente"() {
        given:
            def usuario = new User(username: "usuarioTest", password: "usuarioTest", name: "Usuario", surnames: "Test", email: "usuarioTest@todo.expertojava.ua.es", confirmPassword: false)
            def task1 = new Todo(user: usuario, title:"Todo day before yesterday", date: new Date() - 2, done: true)
            def task2 = new Todo(user: usuario, title:"Todo yesterday", date: new Date() - 1, done: false)
            def task3 = new Todo(user: usuario, title:"Todo today", date: new Date(), done: false)
        and:
            mockDomain(Todo,[task1, task2, task3])
        and:
            def todo1 = service.saveTodo(task1)
            def todo2 = service.saveTodo(task2)
            def todo3 = service.saveTodo(task3)

        expect:
            Todo.count() == 3
        and:
            Todo.list().containsAll([todo1, todo2,todo3])
            todo1.dateDone.format("dd/MM/yyyy") == new Date().format("dd/MM/yyyy")
            todo2.dateDone == null
            todo3.dateDone == null
    }

    void "El método countNextTodos devuelve la cantidad de todos pendientes en los días pasado por parámetro"() {
        given:
            def usuario = new User(username: "usuarioTest", password: "usuarioTest", name: "Usuario", surnames: "Test", email: "usuarioTest@todo.expertojava.ua.es", confirmPassword: false)
            def task1 = new Todo(user: usuario, title:"Todo 1", date: new Date() + 1, done: false)
            def task2 = new Todo(user: usuario, title:"Todo 2", date: new Date() + 2, done: false)
            def task3 = new Todo(user: usuario, title:"Todo 3", date: new Date() + 3, done: false)
            def task4 = new Todo(user: usuario, title:"Todo 4", date: new Date() + 4, done: false)
            def task5 = new Todo(user: usuario, title:"Todo 5", date: new Date() + 5, done: false)
            def task6 = new Todo(user: usuario, title:"Todo 6", date: new Date() + 6, done: false)
            def task7 = new Todo(user: usuario, title:"Todo 7", date: new Date() + 7, done: false)
        and:
            mockDomain(Todo,[task1, task2, task3, task4, task5, task6, task7])
        and:
            def countTodosTres = service.countNextTodos(3)
            def countTodosCinco = service.countNextTodos(5)
            def countTodosSiete = service.countNextTodos(7)
        expect:
            Todo.count() == 7
        and:
            countTodosTres == 3
        and:
            countTodosCinco == 5
        and:
            countTodosSiete == 7
    }
}

package es.ua.expertojava.todo

import grails.test.mixin.*
import spock.lang.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Todo)
class TodoSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    @Unroll
    def "Fecha de recordatorio nunca debe ser posterior a la fecha de vencimiento" () {
        given:
            def todo = new Todo(title:"Todo01", date:new Date() + 1, reminderDate: new Date() + 2);
        when:
            todo.validate();
        then:
            todo.errors['reminderDate']
    }

    @Unroll
    def "No se puede crear una tarea si no se esta logueado" () {
        given:
            def todo = new Todo(title:"Todo01", date:new Date() + 5, reminderDate: new Date() + 2);
        when:
            todo.validate();
        then:
            todo.errors['user']
    }

    @Unroll
    def "Se debe espefificar un usuario valido para crear una tarea" () {
        given:

            def administrador = new User(username: "admin", password: "admin", name: "Administrador", surnames: "Del mundo mundial", email: "administrador@todo.expertojava.ua.es", confirmPassword: false).save();
            def usuario = new User(username: "usuario", password: "usuario", name: "Usuario", surnames: "Huerfano", email: "usuarioHuerfano@todo.expertojava.ua.es", confirmPassword: false).save();

            def todoAdmin = new Todo(user: administrador, title:"Todo01", date:new Date() + 5, reminderDate: new Date() + 2);
            def todoUsuario = new Todo(user: usuario, title:"Todo02", date:new Date() + 5, reminderDate: new Date() + 2);
        when:
            todoAdmin.validate();
            todoUsuario.validate();
        then:
            !todoAdmin.hasErrors();
            !todoUsuario.hasErrors();
    }
}

//static transactional = false def todoService = new TodoService() def setup(){ def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2') controller.springSecurityService = [currentUser : usuario1] controller.todoService = todoService }

// static transactional = false def todoService = new TodoService() def setup(){ def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2') controller.springSecurityService = [currentUser : usuario1] controller.todoService = todoService } def populateValidParams(params) { assert params != null def usuario1 = new User(username: "usuario2",password:"usuario2",name:"usuario2",surnames:"Usuario 2",confirmPassword: "usuario2",email:'usu2@alu.ua.es',dateOfBirth: new Date()-3665,description: 'usuario 2') params["title"] = 'Title name' params["description"] = 'Description name' params["reminderDate"] = new Date()-3 params["date"] = new Date() params["dateDone"] = new Date() params["finalizado"] = true params["user"] = usuario1 }
// reminderDate(nullable:true, validator: { val, obj -> if (val && obj.date) { return val.before(obj?.date) } return true } )
